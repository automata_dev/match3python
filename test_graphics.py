import pygame
import random

WHITE = (255, 255, 255)
COLOR = [(244, 98, 66), (244, 175, 65), (202, 244, 65), (65, 208, 244), (238, 65, 244)]
pad_height = 512
pad_width = 1024


def runGame():
    global surface, clock
    crashed = False
    while not crashed:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                crashed = True
        surface.fill(WHITE)
        for r in range(5):
            for c in range(5):
                pygame.draw.rect(surface, COLOR[random.randrange(0, 5)],
                                 (c * 102, r * 102, 100, 100), 0)

        pygame.display.flip()
        clock.tick(60)
    pygame.quit()


def initGame():
    global surface, clock
    random.seed()
    pygame.init()
    surface = pygame.display.set_mode((pad_width, pad_height))
    pygame.display.set_caption('PyFlying')
    clock = pygame.time.Clock()
    runGame()


initGame()
