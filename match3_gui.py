from match3 import Match3
import pygame
from pygame.locals import *
import random
import numpy as np
import time
import math

"""
    Match3GUI 클래스
"""


class Match3GUI:
    m = None
    surface = None
    bonus = None
    bonus_surface = None
    tile_size = 40
    tile_margin = 2
    sf_tiles = None
    sf_hidden = None
    sf_items = None
    colors = None
    transforms = None
    swap_speed = 2
    drop_speed = 7
    shrinking_speed = 2
    frame_sleep = 0.012
    mouse_event = {}
    mouse_min_dist = 10

    item_images = [
        'swapped.png',
        'ver_line.png',
        'hor_line.png',
        'homing.png',
        'color_marker.png'
    ]

    def __init__(self, dim_y=9, dim_x=9, tile_types=5):
        self.m = Match3(dim_x, dim_y, tile_types)
        self.bonus = Match3(4, 5, 3)
        self.transforms = [[self.default_transform(r, c) for c in range(dim_x)] for r in range(dim_y)]
        self.m.generate_board()
        self.bonus.generate_board()
        self.colors = self.spaced_colors(tile_types)
        # self.colors = []
        # for i in range(tileTypes):
        #     self.colors.append((random.randint(100, 255), random.randint(100, 255), random.randint(100, 255)))
        pygame.init()
        pygame.event.set_allowed([QUIT, MOUSEBUTTONDOWN, MOUSEBUTTONUP, MOUSEMOTION, VIDEORESIZE, KEYUP])
        self.sf_tiles = []
        for i in range(len(self.colors)):
            sf = pygame.Surface((self.tile_size, self.tile_size), HWSURFACE | HWACCEL)
            sf.fill(self.colors[i])
            self.sf_tiles.append(sf)
        tile_width = self.tile_margin + self.tile_size
        tile_height = self.tile_margin + self.tile_size
        self.sf_items = []
        for item_image in self.item_images:
            self.sf_items.append(pygame.image.load('resources/' + item_image))
        self.sf_hidden = pygame.Surface([tile_width * self.m.dim_x + self.tile_margin,
                                         tile_height * self.m.dim_y + self.tile_margin],
                                        HWSURFACE | HWACCEL)
        self.surface = pygame.display.set_mode([tile_width * self.m.dim_x + self.tile_margin,
                                                tile_height * self.m.dim_y + self.tile_margin],
                                               HWSURFACE | HWACCEL | DOUBLEBUF)

    """
        make spaced color
    """

    def spaced_colors(self, n):
        max_value = 16581375  # 255**3
        interval = int(max_value / n)
        colors = [hex(I)[2:].zfill(6) for I in range(150, max_value, interval)]
        return [(int(i[:2], 16), int(i[2:4], 16), int(i[4:], 16)) for i in colors]

    """
        make random color
    """

    def random_colors(self, n):
        ret = []
        r = int(random.random() * 256)
        g = int(random.random() * 256)
        b = int(random.random() * 256)
        step = 256 / n
        for i in range(n):
            r += step
            g += step
            b += step
            r = int(r) % 256
            g = int(g) % 256
            b = int(b) % 256
            ret.append((r, g, b))
        return ret

    """ 
        board information 
    """

    def default_transform(self, r, c):
        size = (self.tile_size + self.tile_margin)
        return r * size, c * size, self.tile_size

    def pixel_to_rc(self, x, y=None):
        if y is None:
            y = x[1]
            x = x[0]
        size = (self.tile_size + self.tile_margin)
        return y // size, x // size

    def board_size(self):
        size = (self.tile_size + self.tile_margin)
        return self.m.dim_y * size, self.m.dim_x * size

    def print_transform(self, size=True, xy=False):
        for r in range(self.m.dim_y):
            for c in range(self.m.dim_x):
                if xy:
                    print(self.transforms[r][c][0], self.transforms[r][c][1], end=' ')
                if size:
                    print(self.transforms[r][c][2], end=' ')
            print()

    """
        get swappable tile
    """

    def get_exchaneable_tile(self, r, c, dr=None):
        if dr is None:
            dr = c
            c = r[1]
            r = r[0]
        # print('get_exchangable_tile dir', dr, 'r', r, 'c', c, 'mouse_event', self.mouse_event)
        if (dr == 'e' and c == self.m.dim_x - 1) or (dr == 'w' and c == 0) or (
                dr == 'n' and r == self.m.dim_y - 1) or (dr == 's' and r == 0):
            return None
        if dr == 'e':
            return r, c + 1
        if dr == 'w':
            return r, c - 1
        if dr == 'n':
            return r + 1, c
        return r - 1, c

    """
        mouse move event handler
    """

    def handle_mouse_move(self, pos):
        if self.mouse_event == {}:
            return
        old_pos = self.mouse_event['pos']
        source = self.mouse_event['rc']
        if 'dir' not in self.mouse_event:
            dist = math.hypot(pos[0] - old_pos[0], pos[1] - old_pos[1])
            if dist > self.mouse_min_dist:  # fire
                angle = math.atan2(pos[1] - old_pos[1], pos[0] - old_pos[0])
                if abs(angle) < math.pi / 4:
                    self.mouse_event['dir'] = 'e'
                elif abs(angle) > math.pi * 3 / 4:
                    self.mouse_event['dir'] = 'w'
                elif angle > 0:
                    self.mouse_event['dir'] = 'n'
                else:
                    self.mouse_event['dir'] = 's'
                target = self.get_exchaneable_tile(self.pixel_to_rc(old_pos), self.mouse_event['dir'])
                # print(source, target)
                self.swap_tiles(source, target)
                self.mouse_event = {}
                if self.check_match() == 0:
                    self.swap_tiles(source, target)
                while self.check_match() > 0:
                    pass

        # if 'dir' not in self.mouse_event:
        #     return

    """
        event handler
    """

    def handle_event(self):
        h, w = self.board_size()
        x, y = pygame.mouse.get_pos()
        pos = (x, h - y)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                # self.surface = None
                # raise Exception('surface closed')
            elif event.type == MOUSEBUTTONDOWN:
                self.mouse_event['pos'] = pos
                self.mouse_event['rc'] = self.pixel_to_rc(pos)
                # print(self.mouse_event)
            elif event.type == MOUSEMOTION:
                self.handle_mouse_move(pos)
            elif event.type == MOUSEBUTTONUP:
                self.mouse_event = {}
            elif event.type == VIDEORESIZE:
                self.surface = pygame.display.set_mode(
                    event.dict['size'], HWSURFACE | DOUBLEBUF | RESIZABLE)
            elif event.type == KEYUP and event.dict['key'] == 112:
                print('------------------------')
                self.m.print_board()
                print('------------------------')
                self.m.print_board(True)
                print('------------------------')
                self.print_transform()
                print('------------------------')
                self.print_transform(size=False, xy=True)

    def swap_tiles(self, s, t):
        self.m.swap_tiles(s, t)
        sy = s[0]
        sx = s[1]
        ty = t[0]
        tx = t[1]
        s_transform = self.transforms[sy][sx]
        self.transforms[sy][sx] = self.transforms[ty][tx]
        self.transforms[ty][tx] = s_transform
        self.animate_pos_size(self.swap_speed)

    def auto(self):
        hint = self.m.find_solution_by_shape()
        self.swap_tiles(hint[0], hint[1])
        # self.m.boom()
        # self.check_match(False)
        while self.check_match() > 0:
            pass

    def check_match(self, update_status=True):
        ret = 0
        if update_status:
            ret = self.m.check_match_by_shape()
        for c in range(self.m.dim_x):
            for r in range(self.m.dim_y):
                if 0 < self.m.status[r][c] < 100 or self.m.status[r][c] > 100:
                    # 사라지는 애니메이션 처리를 위해 크기에 변화를 줌
                    self.transforms[r][c] = (
                        self.transforms[r][c][0], self.transforms[r][c][1], self.transforms[r][c][2] - 1)
        self.animate_pos_size(self.shrinking_speed)
        self.m.remove_marked_tiles()
        """
            set transformation for animation
        """
        for c in range(self.m.dim_x):
            blank = 0
            solid = 0
            for r in range(self.m.dim_y):
                if self.m.board[r][c] == 0:
                    blank += 1
                else:
                    if blank > 0:
                        self.transforms[solid][c] = self.default_transform(r, c)
                    solid += 1
            for r in range(solid, self.m.dim_y):
                self.transforms[r][c] = self.default_transform(self.m.dim_y + r - solid, c)
        self.m.fill_removed_tiles()
        self.animate_pos_size(self.drop_speed)
        return ret

    def draw_board(self):
        self.handle_event()
        tile_height = self.tile_margin + self.tile_size
        self.sf_hidden.fill([0, 0, 0])
        for y in range(self.m.dim_y):
            for x in range(self.m.dim_x):
                tile = self.m.board[y][x]
                size = self.transforms[y][x][2]
                dest = (self.tile_margin + self.transforms[y][x][1]  # dest
                        + (self.tile_size - size) / 2,
                        self.tile_margin + self.board_size()[1] - tile_height - self.transforms[y][x][
                            0]
                        + (self.tile_size - size) / 2,
                        ),
                rect = (0, 0, size, size)
                if 0 < tile < 100:
                    self.sf_hidden.blit(self.sf_tiles[tile - 1], dest, rect)
                elif tile == 102:
                    self.sf_hidden.blit(self.sf_items[1], dest, rect)
                elif tile == 103:
                    self.sf_hidden.blit(self.sf_items[2], dest, rect)
                elif tile == 104:
                    self.sf_hidden.blit(self.sf_items[3], dest, rect)
                elif tile == 105:
                    self.sf_hidden.blit(self.sf_items[4], dest, rect)
                elif tile > 100:
                    self.sf_hidden.blit(self.sf_items[0], dest, rect)
        self.surface.blit(self.sf_hidden, (0, 0))
        pygame.display.update()

    """
        각각의 타일들이 위치를 벗어난 경우 돌아오도록 표현함
    """

    def animate_pos_size(self, speed):
        # self.draw_board()
        dirty = True
        while dirty:
            self.draw_board()
            time.sleep(self.frame_sleep)
            dirty = False
            for y in range(self.m.dim_y):
                for x in range(self.m.dim_x):
                    transform = self.transforms[y][x]
                    tobe = self.default_transform(y, x)
                    if transform != tobe:
                        # position animation
                        dirty = True
                        ny = transform[0] - np.sign(transform[0] - tobe[0]) * speed
                        if abs(ny - tobe[0]) < speed: ny = tobe[0]
                        nx = transform[1] - np.sign(transform[1] - tobe[1]) * speed
                        if abs(nx - tobe[1]) < speed: nx = tobe[1]
                        # size animation
                        size = transform[2]
                        if size == 0:
                            dirty = False
                        elif size < self.tile_size:
                            size -= speed
                            if size < 0: size = self.tile_size
                        self.transforms[y][x] = (ny, nx, size)


def demo():
    pygame.init()
    while True:
        gui.draw_board()
    #      gui.auto()
    #      time.sleep(0.25)

    # for r in range(gui.m.dim_y):
    #     for c in range(gui.m.dim_x):
    #         y, x, s = gui.default_transform(r, c)
    #         gui.transforms[r][c] = (y, x, s - 1)
    # gui.animate_pos_size(1)


if __name__ == '__main__':
    gui = Match3GUI()
    # gui.m.print_board()
    gui.m.generate_board()
    gui.draw_board()
    demo()

    # for epoch in range(0):
    #     win = 0
    #     combo = 0
    #     rewardAccum = 0
    #     for i in range(1):
    #         if not m.findSolution():
    #             m.generateBoard()
    #         hint = m.findSolutionByShape()
    #         m.swapTiles(hint[0][0], hint[0][1], hint[1][0], hint[1][1])
    #         reward = m.checkMatchByShape()
    #         rewardAccum += reward
    #         win += 1 if reward > 0 else 0
    #         while reward > 0:
    #             combo += 1
    #             m.removeMatchedTiles()
    #             m.fillNewTiles()
    #             reward = m.checkMatchByShape()
    #     print('epoch:', epoch + 1, 'reward:', rewardAccum, 'mean reward:', rewardAccum / (i + 1), end=' ')
    #     if win > 0:
    #         print('win:', win, 'mean win:', win / (i + 1), 'combo', combo, 'mean combo:', combo / win, end='')
    #     print()
    #
    # import pygame
    #
    # pygame.init()
    # surface = pygame.display.set_mode([800, 600])
    # surface.fill([255, 255, 255])
    # pygame.draw.rect(surface, (250, 200, 200), (0, 0, 52, 52), 1)
    # pygame.display.update()
    # surface.fill([0, 0, 0])
    # pygame.draw.rect(surface, (250, 200, 200), (0, 0, 52, 52), 1)
    # pygame.display.update()
