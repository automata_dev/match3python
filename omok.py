import numpy as np


class Omok:
    dimX = 0
    dimY = 0
    minMatch = 0
    board = []
    next = 1
    templates = None

    def __init__(self, dimX=3, dimY=3, minMatch=3):
        self.dimX = dimX
        self.dimY = dimY
        self.minMatch = minMatch
        self.templates = np.array([
            np.zeros((self.minMatch, 1), dtype='int32') + 1,
            np.zeros((1, self.minMatch), dtype='int32') + 1,
            np.eye(self.minMatch, dtype='int32', order='C'),
            np.fliplr(np.eye(self.minMatch, dtype='int32', order='C'))
        ])

    def generateBoard(self):
        self.board = np.zeros((self.dimY, self.dimX), dtype='int32')

    def makeMove(self, r, c, side):
        # 착수 가능 체크
        if self.board[r][c] != 0 or self.next != side:
            return -1
        self.board[r][c] = side
        # debug
        # self.printBoard()
        # 승리조사
        result = self.checkWinner(side)
        if result == side:
            # print(side, 'win!')
            return -2
        elif result == 3:
            print('draw!')
            return -3
        # 다음사람
        self.next = 2 if side == 1 else 1
        return self.next

    def checkWinner(self, side):
        checkers = self.templates * side
        draw = True
        for checker in checkers:
            width = checker.shape[1]
            height = checker.shape[0]
            win = False
            for r in range(self.dimY - height + 1):
                for c in range(self.dimX - width + 1):
                    match = 0
                    for winr in range(height):
                        for winc in range(width):
                            if checker[winr][winc] != 0:
                                if checker[winr][winc] == self.board[r + winr][c + winc]:
                                    match += 1
                                if self.board[r + winr][c + winc] == 0:
                                    draw = False
                        if match == self.minMatch:
                            win = True
            if win:
                return side
        if draw:
            return 3
        return 0

    def printBoard(self):
        print('+-' * self.dimX, end='')
        print('+')
        for r in range(self.dimY):
            for c in range(self.dimX):
                cell = '| '
                if self.board[r][c] == 1:
                    cell = '|O'
                elif self.board[r][c] == 2:
                    cell = '|X'
                print(cell, end='')
            print('|')
            print('+-' * self.dimX, end='')
            print('+')

    def makeMoveWithCellNumber(self, cellNum, side):
        denominator = self.dimX
        r = cellNum // denominator
        c = cellNum % denominator
        return self.makeMove(r, c, side)


if __name__ == '__main__':
    m = Omok()
    m.generateBoard()

