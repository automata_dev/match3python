"""@package match3
Match3 게임용 클래스.
More details.
"""
import random
import numpy as np

random.seed()

"""
    Match3 클래스
"""


class Match3:
    tile_types = 5
    dim_x = 9
    dim_y = 9
    board = None
    status = None
    match_transform = None
    solution_transfrom = None
    SWAPPED = 100

    """
        @brief 기본 9x9 , 5종 타일을 기본값으로 게임을 초기화한다.
    """

    def __init__(self, dim_y=9, dim_x=9, tile_types=5):
        self.dim_y = dim_y
        self.dim_x = dim_x
        self.tile_types = tile_types
        # self.generateBoard()

    """
        파라미터에 의해 보드를 생성한다.
    """

    def generate_board(self):
        self.clear_status()
        self.board = np.array(
            [[random.randrange(1, self.tile_types + 1) for c in range(self.dim_x)] for r in range(self.dim_y)],
            dtype='int32')
        reward = self.check_match_by_shape()
        while reward > 0:
            self.remove_marked_tiles()
            self.fill_removed_tiles()
            reward = self.check_match_by_shape()
            result = self.find_solution_by_shape()
            print(reward, result)

    """
        생성된 보드를 출력한다.
    """

    def print_board(self, status=False):
        for r in range(self.dim_y - 1, -1, -1):
            for c in range(self.dim_x):
                if status:
                    print("{0:01d}".format(self.status[r][c]), end='  ')
                else:
                    print("{0:01d}".format(self.board[r][c]), end='  ')
            print()

    """
        모든 상태를 0으로 초기화한다.
    """

    def clear_status(self):
        self.status = np.zeros((self.dim_y, self.dim_x), dtype='int32')

    """
           두개의 타일을 교환한다.
    """

    def swap_tiles(self, s, t):
        sy = s[0]
        sx = s[1]
        ty = t[0]
        tx = t[1]
        t = self.board[sy][sx]
        self.board[sy][sx] = self.board[ty][tx]
        self.board[ty][tx] = t
        # s = self.status[sy][sx]
        # self.status[sy][sx] = self.status[ty][tx]
        # self.status[ty][tx] = s
        self.status[sy][sx] = self.SWAPPED
        self.status[ty][tx] = self.SWAPPED

    """
        행열 인덱스가 아닌 경계선 번호를 주어서 타일을 서로 바꾼다
    """

    def swap_tiles_by_wall_number(self, wall_num):
        magic = (self.dim_x - 1) * self.dim_y
        direction = 1 if wall_num < magic else 3
        denominator = self.dim_x - 1 if wall_num < magic else self.dim_x
        wall_num %= magic
        r = wall_num // denominator
        c = wall_num % denominator
        # print(r, c, direction, sep=',')
        tr = r
        tc = c
        if direction == 1:
            tc += 1
        else:
            tr += 1
        self.swap_tiles(r, c, tr, tc)

    """
        해법 패턴을 생성하여 가능성을 찾는다.
    """

    def find_solution_by_shape(self):
        # available shapes
        templates = []
        templates.append([[[0, 0, 1, 0, 0], [1, 1, 0, 1, 1], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]],
                          [[0, 0, 1, 0, 0], [0, 0, 1, 0, 0], [0, 0, 0, 0, 0], [0, 0, 0, 0, 0]]])
        templates.append([[[1, 0, 0], [0, 1, 1], [1, 0, 0], [1, 0, 0]],
                          [[1, 0, 0], [1, 0, 0], [0, 0, 0], [0, 0, 0]]])
        templates.append([[[0, 0, 1, 0], [1, 1, 0, 1], [0, 0, 1, 0], [0, 0, 1, 0]],
                          [[0, 0, 1, 0], [0, 0, 1, 0], [0, 0, 0, 0], [0, 0, 0, 0]]])
        templates.append([[[1, 1, 0, 1, 1], [0, 0, 1, 0, 0]],
                          [[0, 0, 1, 0, 0], [0, 0, 1, 0, 0]]])
        templates.append([[[1, 0, 1, 1], [0, 1, 0, 0]],
                          [[0, 1, 0, 0], [0, 1, 0, 0]]])
        templates.append([[[1, 0, 1], [0, 1, 0]],
                          [[0, 1, 0], [0, 1, 0]]])
        templates.append([[[1, 1, 0], [0, 0, 1]],
                          [[0, 0, 1], [0, 0, 1]]])
        templates.append([[[1, 1, 0, 1]],
                          [[0, 0, 1, 1]]])
        if self.solution_transfrom is None:
            self.solution_transfrom = []
            for pattern in templates:
                self.solution_transfrom.append(self.make_transformed_array(pattern, (1, 2)))
        colors = list(range(1, self.tile_types + 1))
        pattern, solution, r, c, ti, ci = self.compare_board_by_template(self.solution_transfrom, colors,
                                                                         return_first=True)
        # print(pattern, solution, r, c, ti, ci)
        ret = []
        for y in range(len(solution)):
            row = solution[y]
            for x in range(len(row)):
                if solution[y][x] == 1:
                    ret.append((y + r, x + c))
                    if len(ret) == 2:
                        return ret

    """
        매치패턴을 생성하여 보드의 타일의 매치값을 조사한다.
    """

    def check_match_by_shape(self):
        # available shapes
        templates = []
        templates.append([[[1, 1, 1, 1, 1], [0, 0, 1, 0, 0], [0, 0, 1, 0, 0]], 109])
        templates.append([[[1, 1, 1], [0, 0, 1], [0, 0, 1]], 108])
        templates.append([[[1, 1, 1, 1], [0, 0, 1, 0], [0, 0, 1, 0]], 107])
        templates.append([[[1, 1, 1], [0, 1, 0], [0, 1, 0]], 106])
        templates.append([[[1, 1, 1, 1, 1]], 105])
        templates.append([[[1, 1], [1, 1]], 104])
        templates.append([[[1, 1, 1, 1]], 102])
        templates.append([[[1, 1, 1]], 101])
        if self.match_transform is None:
            self.match_transform = []
            for pattern in templates:
                self.match_transform.append(self.make_transformed_array(pattern[0], appendix=pattern[1]))
        colors = list(range(1, self.tile_types + 1))
        return self.compare_board_by_template(self.match_transform, colors)

    """
        배열을 회전/반전하여 생성된 고유한 변형배열을 돌려준다
    """

    def make_transformed_array(self, array, axes=(0, 1), appendix=None):
        ret = [array]
        if appendix is not None:
            ret = [[array, appendix]]
        for angle in range(3):
            array = np.rot90(array, axes=axes)
            unique = True
            for el in ret:
                if np.array_equal(array, el):
                    unique = False
                    break
            if unique:
                if appendix is not None:
                    appender = array.tolist()
                    ret.append([appender, appendix])
                else:
                    ret.append(array)
        array = np.flip(array, len(array.shape) - 1)
        for angle in range(4):
            array = np.rot90(array, axes=axes)
            unique = True
            for el in ret:
                if np.array_equal(array, el):
                    unique = False
                    break
            if unique:
                if appendix is not None:
                    appender = array.tolist()
                    ret.append([appender, appendix])
                else:
                    ret.append(array)
        return ret

    """
        주어진 패턴을 이용해서 매치를 찾는 방법
        패턴에 타일번호를 바꾸고 각도를 바꿔서 전체보드를 패턴 윈도우로 비교한다.
        윈도우는 보드 내의 주어진 범위안에서 움직인다.
    """

    def compare_board_by_template(self, templates, colors, return_first=False):
        # self.clear_status()
        _reward = 0
        for ti in range(len(templates)):
            comparers = templates[ti]
            for ci in range(len(comparers)):
                comparer = comparers[ci][0]
                solution = comparers[ci][1]
                reward = 0
                if type(reward) is int:
                    comparer = comparers[ci][0]
                    reward = comparers[ci][1]

                for c in colors:
                    t = np.array(comparer) * c
                    for y in range(self.dim_y - t.shape[0] + 1):
                        for x in range(self.dim_x - t.shape[1] + 1):
                            compared_board = self.board[y: y + t.shape[0], x: x + t.shape[1]]
                            compared_status = self.status[y: y + t.shape[0], x: x + t.shape[1]]
                            match = True
                            for iy in range(t.shape[0]):
                                for ix in range(t.shape[1]):
                                    if t[iy][ix] != 0:
                                        # if t[iy][ix] != compared_board[iy][ix] or compared_status[iy][ix] != 0:
                                        if t[iy][ix] != compared_board[iy][ix]:
                                            match = False
                            if match:
                                # 첫번째 패턴 매치만 찾아서 알려줄 경우
                                if return_first:
                                    return t, solution, y, x, ti, ci
                                _reward += reward
                                for iy in range(t.shape[0]):
                                    for ix in range(t.shape[1]):
                                        if t[iy][ix] != 0:
                                            if compared_status[iy][ix] == 0:
                                                compared_status[iy][ix] = c
                                            elif compared_status[iy][ix] == self.SWAPPED:  # swapped tile?
                                                compared_status[iy][ix] = reward  # item
                                                if reward == 102 and t.shape[0] == 4: # vertical item
                                                    compared_status[iy][ix] += 1
                                                print('reward', reward, 'shape', t.shape)
        return _reward

    """
         수직 혹은 수평의 타일을 제거한다
     """

    def mark_along_axis(self, r=None, c=None):
        if r is not None:
            for c in range(self.dim_x):
                self.status[r][c] = 1
        elif c is not None:
            for r in range(self.dim_y):
                self.status[r][c] = 1
        else:
            raise Exception('row or column must be assgined')

    """
        반경 내의 모든 타일을 제거한다
    """

    def mark_area(self, radius=None):
        for r in range(self.dim_y):
            for c in range(self.dim_x):
                self.status[r][c] = 1

    """
        특정 색상의 타일을 제거한다
    """

    def mark_color(self, color):
        for r in range(self.dim_y):
            for c in range(self.dim_x):
                if self.board[r][c] == color:
                    self.status[r][c] = 1

    """
        매치가 발생한 타일의 값을 0으로 만들어서 지운다
    """

    def remove_marked_tiles(self):
        for r in range(self.dim_y):
            for c in range(self.dim_x):
                if self.status[r][c] == 0:
                    pass
                elif 0 < self.status[r][c] < 100:
                    self.board[r][c] = 0
                elif self.status[r][c] == 100:
                    pass
                elif self.status[r][c] > 101:
                    self.board[r][c] = self.status[r][c]
                    self.status[r][c] = 0

    """
        제거된 타일은 새로운 타일값으로 채운다. 이때 Y 방향 중력을 적용하여 제거된
        자리에 Y 값이 큰 타일을 내려놓고 빈공간에 새 타일을 넣는다
    """

    def fill_removed_tiles(self):
        self.clear_status()
        for c in range(self.dim_x):
            blank = 0
            solid = 0
            for r in range(self.dim_y):
                if self.board[r][c] == 0:
                    blank += 1
                else:
                    if blank > 0:
                        self.swap_tiles((r, c), (solid, c))
                    solid += 1
            for r in range(solid, self.dim_y):
                self.board[r][c] = random.randrange(1, self.tile_types + 1)

    """
        just helper function
    """

    def next(self):
        score = self.check_match_by_shape()
        while score > 0:
            print('score', score)
            self.print_board()
            self.remove_marked_tiles()
            print('-------------------------')
            self.print_board()
            print('-------------------------')
            self.fill_removed_tiles()
            score = self.check_match_by_shape()


"""
    메인 모듈일 경우 수행됨
"""
if __name__ == '__main__':
    print('3 match game module. this is not for standalone game')
