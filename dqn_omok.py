import numpy as np
import zoo
import time

from keras.models import Sequential
from keras.layers import Dense, Activation, Flatten
from keras.optimizers import Adam

from rl.agents.dqn import DQNAgent
from rl.policy import BoltzmannQPolicy
from rl.memory import SequentialMemory


ENV_NAME = 'Omok-v0'
#ENV_NAME = 'CartPole-v0'


# Get the environment and extract the number of actions.
env = zoo.make(ENV_NAME)
np.random.seed(123)
env.seed(123)
nb_actions = env.action_space.n

# Next, we build a very simple model.
model1 = Sequential()
model1.add(Flatten(input_shape=(1,) + env.observation_space.shape))
model1.add(Dense(16))
model1.add(Activation('relu'))
model1.add(Dense(16))
model1.add(Activation('relu'))
model1.add(Dense(16))
model1.add(Activation('relu'))
model1.add(Dense(nb_actions))
model1.add(Activation('linear'))
# print(model.summary())
model2 = Sequential()
model2.add(Flatten(input_shape=(1,) + env.observation_space.shape))
model2.add(Dense(16))
model2.add(Activation('relu'))
model2.add(Dense(16))
model2.add(Activation('relu'))
model2.add(Dense(16))
model2.add(Activation('relu'))
model2.add(Dense(nb_actions))
model2.add(Activation('linear'))


memory1 = SequentialMemory(limit=5000, window_length=1)
policy1 = BoltzmannQPolicy()
dqn1 = DQNAgent(model=model1, nb_actions=nb_actions, memory=memory1, nb_steps_warmup=10,
                target_model_update=1e-2, policy=policy1)
dqn1.compile(Adam(lr=1e-3), metrics=['mae'])

memory2 = SequentialMemory(limit=5000, window_length=1)
policy2 = BoltzmannQPolicy()
dqn2 = DQNAgent(model=model2, nb_actions=nb_actions, memory=memory2, nb_steps_warmup=10,
                target_model_update=1e-2, policy=policy2)
dqn2.compile(Adam(lr=1e-3), metrics=['mae'])

dqn1.load_weights('dqn_{}_weights.h5f'.format(ENV_NAME))

# Okay, now it's time to learn something! We visualize the training here for show, but this
# slows down training quite a lot. You can always safely abort the training prematurely using
# Ctrl + C.
# try:
dqn1.training = True
dqn2.training = True
try:
    for i in range(2):
        state = env.reset()
        while True:
            agent = None;
            if state[0] == 1:
                agent = dqn1
            elif state[0] == 2:
                agent = dqn2
            act = agent.forward(state)
            state, reward, end, info = env.step(act)
            agent.backward(reward, end)
            print('agent act', act, 'reward:', reward)
            env.render('human')
            if end: break
except:
    pass
# except:
#     print('------ exception')

# After training is done, we save the final weights.
print('----- saving weight')
dqn1.save_weights('dqn_{}_weights.h5f'.format(ENV_NAME), overwrite=True)

# Finally, evaluate our algorithm for 5 episodes.
# dqn.test(env, nb_episodes=10, visualize=True)
