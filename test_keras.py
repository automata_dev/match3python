from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Flatten
from keras.layers import Convolution2D, MaxPooling2D
import random
import numpy as np
from match3 import Match3

model = Sequential()
model.add(Dense(10, activation='relu', input_shape=(10,)))
model.add(Dense(10, activation='softmax'))
model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])
y = np.array([[0, 1, 0, 0, 0, 0, 0, 0, 0, 0]])
for i in range(1, 1000):
    x = np.array([random.sample(range(1, 100), 10)])
    model.fit(x, y, batch_size=1, epochs=1)
x = np.array([random.sample(range(1, 100), 10)])
p = model.predict(x)
print('prediction', p)
