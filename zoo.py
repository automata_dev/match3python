import random
import numpy as np
from match3 import Match3
from omok import Omok
import pygame
import time


class EnvOmok:
    class ActionSpace:
        n = 9;

    class ObservationSpace():
        shape = (10,)

    surface = None
    clock = None
    m = None
    side = 1
    action_space = ActionSpace()
    observation_space = ObservationSpace()

    def seed(self, s):
        random.seed(s)
        pygame.init()
        self.clock = pygame.time.Clock()

    def reset(self):
        self.m = Omok(3, 3, 3)
        self.m.generateBoard()
        time.sleep(0.1)
        return np.append(self.side, self.m.board.reshape(9))

    def step(self, action):
        result = self.m.makeMoveWithCellNumber(action, self.side)
        # -1: wrong, -2 :somebody win, -3: draw 1,2 : side
        if result > 0: self.side = result
        reward = 0
        if result == -2:
            reward = 10
        elif result == -3:
            reward = 1
        elif result == -1:
            reward = -1
        return (np.append(self.side, self.m.board.reshape(9)), reward, result in [-2, -3], {})

    def render(self, mode):
        pass
        if self.surface is None:
            self.surface = pygame.display.set_mode([self.m.dimX * 52, self.m.dimY * 52])

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise Exception('surface closed')

        self.surface.fill([255, 255, 255])
        for y in range(self.m.dimY):
            for x in range(self.m.dimX):
                pygame.draw.rect(self.surface, (200, 200, 200), (x * 52 + 1, y * 52 + 1, 52, 52), 1)
                if self.m.board[y][x] == 1:
                    pygame.draw.circle(self.surface, (150, 150, 150), (x * 52 + 25, y * 52 + 25), 20, 2)
                elif self.m.board[y][x] == 2:
                    pygame.draw.line(self.surface, (150, 150, 150), (x * 52 + 5, y * 52 + 5),
                                     (x * 52 + 45, y * 52 + 45), 2)
                    pygame.draw.line(self.surface, (150, 150, 150), (x * 52 + 5, y * 52 + 44),
                                     (x * 52 + 44, y * 52 + 5), 2)
        pygame.display.update()
        # time.sleep(0.1)


class EnvMatch3:
    class ActionSpace:
        n = 144;

    class ObservationSpace():
        shape = (4, 4)

    action_space = ActionSpace()
    observation_space = ObservationSpace()
    game = None
    episodeSize = 20
    tries = 0
    surface = None
    WHITE = (255, 255, 255)
    COLOR = [(244, 98, 66), (244, 175, 65), (202, 244, 65), (65, 208, 244), (238, 65, 244)]
    clock = None

    def seed(self, s):
        random.seed(s)
        pygame.init()
        self.clock = pygame.time.Clock()

    def reset(self):
        self.game = Match3(4, 4, 3)
        self.game.generate_board()
        return self.game.board / 4

    def step(self, action):
        self.game.swap_tiles_by_wall_number(action)
        reward = self.game.check_match_by_shape()
        rewardTotal = reward
        combo = 0
        while reward > 0:
            combo += 1
            self.game.remove_marked_tiles()
            self.game.fiilNewTiles()
            reward = self.game.check_match_by_shape()
            rewardTotal += reward
        done = False
        if not self.game.find_solution():
            done = True
        self.tries += 1
        if self.tries == self.episodeSize:
            done = True
            self.tries = 0
        return (self.game.board / 4, rewardTotal, done, {})

    def render(self, mode):
        pass
        if self.surface is None:
            self.surface = pygame.display.set_mode([self.game.dim_x * 52, self.game.dim_y * 52])

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                raise Exception('surface closed')

        self.surface.fill(self.WHITE)
        for y in range(self.game.dim_y - 1, -1, -1):
            for x in range(self.game.dim_x):
                pygame.draw.rect(self.surface, self.COLOR[self.game.board[y][x]], (x * 52 + 1, y * 52 + 1, 50, 50))
        pygame.display.update()
        # time.sleep(0.1)


def make(name):
    if name == 'Match3-v0':
        return EnvMatch3()
    elif name == 'Omok-v0':
        return EnvOmok()
    raise Exception('No such environment ' + name)


if __name__ == '__main__':
    env = make('Omok-v0')
    env.reset()
